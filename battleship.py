
#from mpi4py import MPI
import numpy as np

#set size of board
board_size = [10,10]

def main():
    
    # Initialize the MPI execution environment
    comm = MPI.COMM_WORLD
    
    rank = comm.Get_rank()    
    
    #check that there are exactly 2 processors
    size = comm.Get_size()    
    if size != 2:
        if rank == 0:
            print "Must be exactly 2 players. Please set the number of processes to 2"
        return()
    
    # 1) set up board for local process. Place the ship
    my_board = np.zeros(board_size)
    
    # 2) Set up of board to record guesses.
    opponents_board = np.zeros(board_size)
    guess_list = {'locations' : [], #locations holds a list of x,y coordinates where local processes guessed
                  'results'   : []}   #results holds a list of the hit/miss status of each guess

    # make sure setup is finished
    comm.Barrier()

    # 3) Set up game status information
    whose_turn = np.random.randint(0,size,1)
    game_running = True
    winner = np.zeros(1,dtype=int)
    guess_location = np.zeros(2,dtype=int)
    guess_result = np.zeros(1,dtype=int)
    
    while game_running:
        
        # decide whose turn it is. With two players, just switch between 0 and 1
        whose_turn = (whose_turn + 1) % size
        
        # 4) if it's my turn, make a guess
        
        # 5) communicate the guess. Update guess_location        
        
        # 6) if I'm the target, score the guess
            #if the ship is sunk, stop the game
            game_running = False
            winner = whose_turn     #The winner will be the player who is guessing now
                    
        # 7) communicate the result of the guess. Update guess_result. If I'm the guessor, update guess_list

        # 8) Broadcast the state of the game. Everyone should know game_running, winner. Sent by the target.                    
        

    # Give the final message
    if rank == 0:
        print 'Player', winner, 'won!'
    
    return()


        
                  
                  

if __name__ == '__main__':
    main()
    